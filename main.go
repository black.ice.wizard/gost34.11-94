package main

import (
	"encoding/hex"
	"fmt"
	"github.com/urfave/cli"
	"go.cypherpunks.ru/gogost/v4/gost341194"
	"io/ioutil"
	"os"
)

func main() {
	app := &cli.App{
		Name:    "hash_gost",
		Usage:   "Print GOST 34.11-94 (265-bit) checksum",
		Version: "v0.0.1",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "file, f",
				Usage: "file name from hash sum",
			},
		},
		Action: func(c *cli.Context) error {
			arguments := c.Args().Get(0)
			data := []byte(c.Args().Get(0))
			if arguments == "" && !c.IsSet("file") {
				return cli.ShowAppHelp(c)
			}
			if c.IsSet("file") {
				fileName := c.String("file")
				data = getFileData(fileName)
			}
			fmt.Println(getHash(&data))
			return nil
		},
	}

	err := app.Run(os.Args)

	if err != nil {
		fmt.Println(err)
	}
}

func getHash(data *[]byte) string {
	h := gost341194.New(gost341194.SboxDefault)
	h.Write(*data)
	return hex.EncodeToString(h.Sum(nil))
}

func getFileData(path string) []byte {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println("error: ", err)
		os.Exit(1)
	}
	file.Close()
	content, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println("error: ", err)
		os.Exit(1)
	}
	return content
}
